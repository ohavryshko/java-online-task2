package com.epam.havryshko_orest;

class Party {

    private boolean[] guestsOnParty;
    private int rangeFromBobToLastGuest, currentPerson, selectedRandomPerson;
    private boolean alreadyHeardRumor;

    boolean[] startParty(int amountGuests) {
        guestsOnParty = new boolean[amountGuests];
        System.out.println("Let the party begin!");
        System.out.println("Bob knows a rumor about ALice! It is getting hot!");
        guestsOnParty[0] = true;
        return guestsOnParty;
    }

    boolean[] spreadRumor(boolean[] guestsOnParty) {
        rangeFromBobToLastGuest = (guestsOnParty.length - 1) + 1;
        currentPerson = 0;
        alreadyHeardRumor = false;
        while (!alreadyHeardRumor) {
            selectedRandomPerson = (int) (Math.random() * rangeFromBobToLastGuest);
            if (selectedRandomPerson == currentPerson) {
                while (selectedRandomPerson == currentPerson) {
                    selectedRandomPerson = (int) (Math.random() * rangeFromBobToLastGuest);
                }
            }
            if (!guestsOnParty[selectedRandomPerson]) {
                guestsOnParty[selectedRandomPerson] = true;
            } else {
                alreadyHeardRumor = true;
            }
        }
        return guestsOnParty;
    }

    boolean allHearRumor(boolean[] guestsOnParty) {
        for (boolean b : guestsOnParty) {
            if (guestsOnParty[0] != b) {
                return false;
            }
        }
        return true;
    }
}
