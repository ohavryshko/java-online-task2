package com.epam.havryshko_orest;

import java.util.Scanner;

class Attempt {

    private int allGuestsHearRumor = 0;
    private int allAttempts, amountGuests;
    private double possibilityAllHearRumor;
    private boolean[] startedParty;
    private Party party;
    private Scanner scanner;

    void enterGuestsAndAttempts() {
        scanner = new Scanner(System.in);
        System.out.println("Enter the number of attempts to check the possibility. You will get better result with more attempts (> 1000)");
        allAttempts = scanner.nextInt();
        if (allAttempts <= 0) {
            System.out.println("Don't cheat! Attempts should greater 0!");
            enterGuestsAndAttempts();
        }
        System.out.println("Please, enter the amount of the people at the party. Amount should be > 2");
        amountGuests = scanner.nextInt();
        if (amountGuests <= 2) {
            System.out.println("I warned you! Amount of guests should be > 2");
            enterGuestsAndAttempts();
        }
        scanner.close();
    }

    void allHeardRumorAttempts() {

        for (int i = 0; i < allAttempts; i++) {
            party = new Party();
            startedParty = party.startParty(amountGuests);
            startedParty = party.spreadRumor(startedParty);
            if (party.allHearRumor(startedParty)) {
                System.out.println("Bob did this! All the guests heard rumor about Alice!");
                allGuestsHearRumor++;
            } else {
                System.out.println("Bob failed! All guests did not hear rumor about Alice!");
            }

        }
    }

    void calculatePossibility() {
        possibilityAllHearRumor = ((double) allGuestsHearRumor) / ((double) allAttempts);
        System.out.println("Possibility that all guests heard the rumor about Alice for " + allAttempts + " attempts is: " + possibilityAllHearRumor);
    }

}
