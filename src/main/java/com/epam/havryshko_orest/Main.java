package com.epam.havryshko_orest;

public class Main {
    public static void main(String[] args) {
        Attempt attempt = new Attempt();
        attempt.enterGuestsAndAttempts();
        attempt.allHeardRumorAttempts();
        attempt.calculatePossibility();
    }
}
